### Contexte
Sujet dans le cadre d'un projet en "Génie logiciel - Framework"

### Fonctionnalités

1. Un apiculteur accède à son compte
2. Un apiculteur ajoute une ruche
3. Un apiculteur consulte une ruche
4. Un apiculteur ajoute une intervention
5. Un apiculteur retire une ruche
6. Un apiculteur ajoute une récolte
